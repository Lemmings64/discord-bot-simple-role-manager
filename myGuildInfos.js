module.exports = class myGuild {
	constructor(bot) {
		this.bot = bot;
		this.testString = '_TEST'; //''; // Add _TEST at end of constant for dev testing
		this.GUILD_ID = '702199599019982848'; // WTFake
		this.GUILD_ID_TEST = '381021639359463424'; // Coin du casu
		this.SALON_RULES_ID = '702821639863140449'; // Salon Rules of WTFake
		this.SALON_RULES_ID_TEST = '638359902628347907'; // Salon test of Casu
		this.MESSAGE_RULES_ID = '705723470197161994'; // Id of message with rules
		this.MESSAGE_RULES_ID_TEST = '700729910356082718'; //Id of message ?? of Casu
		this.ROLE_GITD_ID = '714409461514502186'; // Id of the rule GITD of WTFake
		this.ROLE_RULES_OK_ID = '???'; // Id of the membres-accepted-role of WTFake
		this.ROLE_RULES_OK_ID_TEST = '585397799236141056'; // Id of the casu role from Casu guild
		this.EMOJI_RULES_OK_NAME = '👍'; // Name of the thumbs up emoji
		this.EMOJI_RULES_OK_NAME_TEST = 'spinda'; // name of the spinda eoji on Casu guild
	}
	get guild() {
		return this.bot.guilds.cache.get(this['GUILD_ID'+this.testString]);
	}
	get salon() {
		return this.bot.channels.cache.get(this['SALON_RULES_ID'+this.testString]);
	}
	get role() {
		return this.guild.roles.cache.get(this['ROLE_RULES_OK_ID'+this.testString]);
	}
	get message() {
		return null;
	}
	get emojiOk() {
		return this['EMOJI_RULES_OK_NAME'+this.testString];
	}
};