var Discord = require('discord.js');
var logger = require('winston');
var auth = require('./auth.json');
const _ = require('lodash');
var MyGuild = require('./myGuildInfos.js');

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
const bot = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });
var myGuild = {};

bot.on('ready', function (evt) {
    logger.info('Connected');
	myGuild = new MyGuild(bot);
});
bot.on('message', function (message) {
    if (message.content === '!ping') {
		message.reply('pong').then(msg => {
			msg.delete({ timeout: 1000 });
			message.delete({ timeout: 1000 });
		}).catch(console.error);
    }
});

bot.on('messageReactionAdd', (reaction, user) => {
	// handle the case where the event is partial (msg deleted, etc ...)
	if (reaction.partial) {
		reaction.fetch().then(fullReaction => {
		// add the role to the members who react with the correct emoji on rules message
			addRole(fullReaction, user, myGuild.role, myGuild.message, null, myGuild.emojiOk);
		}).catch(error => {
			console.log('Error in fetching partials : ', error);
		});
	}else{
		// add the role to the members who react with the correct emoji on rules message
		addRole(reaction, user, myGuild.role, myGuild.message, null, myGuild.emojiOk);
	}
});

function addRole (reaction, user, role, msg, author, emojiName){
	if (msg && !reaction.message.author.username === msg) return;
	if (author && !reaction.message.author.username === author) return;
	if (emojiName && !_.includes(reaction.emoji.name, emojiName)) return;
	
	myGuild.guild.members.fetch(user).then(member => {
		member.roles.add(role);
	});
}

bot.on('messageReactionRemove', (reaction, user) => {
	// handle the case where the event is partial (msg deleted, etc ...)
	if (reaction.partial) {
		reaction.fetch().then(fullReaction => {
		// Remove the role to the members who un-react with the correct emoji on rules message
			RemoveRole(fullReaction, user, myGuild.role, myGuild.message, null, myGuild.emojiOk);
		}).catch(error => {
			console.log('Error in fetching partials : ', error);
		});
	}else{
		// Remove the role to the members who un-react with the correct emoji on rules message
		RemoveRole(reaction, user, myGuild.role, myGuild.message, null, myGuild.emojiOk);
	}
});

function RemoveRole (reaction, user, role, msg, author, emojiName){
	if (msg && !reaction.message.author.username === msg) return;
	if (author && !reaction.message.author.username === author) return;
	if (emojiName && !_.includes(reaction.emoji.name, emojiName)) return;
	
	myGuild.guild.members.fetch(user).then(member => {
		member.roles.remove(role);
	});
}

async function sendAndDeleteAsync (canal, msg) {
	await canal.send(msg).then(msg => msg.delete({ timeout: 7200000 })).catch('Error while posting/deleting message');
}

async function sendAsync (canal, msg) {
	await canal.send(msg).catch('Error while posting message');
}

bot.login(auth.token);
